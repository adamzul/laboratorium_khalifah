<?php

namespace app\controllers;

use Yii;
use app\models\DetailPemeriksaan;
use app\models\DetailPemeriksaanSearch;
use app\models\HasilPemeriksaan;
use app\models\JenisPemeriksaan;
use App\Controllers\HasilPemeriksaanController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * DetailPemeriksaanController implements the CRUD actions for DetailPemeriksaan model.
 */
class DetailPemeriksaanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                        'actions'=>[
                            'index',
                            'create',
                            'update',
                            'delete',
                            'view'
                        ],
                        'allow'=>true,
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity->role=='1'
                            );
                        }
                    ],
               
                    [
                        'actions'=>[
                            'index',
                            'create',
                            'update',
                            'delete',
                            'view'
                        ],
                        'allow'=>true,
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity->role=='2'
                            );
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DetailPemeriksaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DetailPemeriksaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DetailPemeriksaan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DetailPemeriksaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($param=null)
    {
        $model = new DetailPemeriksaan();
        $hasilPemeriksaan=HasilPemeriksaan::findOne(['id'=>$param]);

        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            $hasilPemeriksaan=HasilPemeriksaan::findOne(['id'=>$param]);
         $detailPemeriksaan= DetailPemeriksaan::findAll(['id_hasil_pemeriksaan' => $param ]);
            $i=0;
            $bayar=0;
            foreach($detailPemeriksaan as $detail)
            {

                $arrayOfId[$i]=$detail->id_jenis_pemeriksaan;
                $bayar=$bayar+JenisPemeriksaan::find(["id"=>$arrayOfId[$i]])->sum('harga');

                $i=$i+1;
            }
            $hasilPemeriksaan->total_bayar=$bayar;
            $hasilPemeriksaan->load(Yii::$app->request->post()) ;
            $hasilPemeriksaan->save();           
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'hasilPemeriksaan' => $hasilPemeriksaan,
                'param' => $param,
                'arrayOfId' => $hasilPemeriksaan->total_bayar,
            ]);
        }
    }

    /**
     * Updates an existing DetailPemeriksaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DetailPemeriksaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DetailPemeriksaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DetailPemeriksaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DetailPemeriksaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actiontambahTotalbayar($param=0)
    {
        
    }
}
