-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2016 at 11:05 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lab_rumahsakit`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pemeriksaan`
--

CREATE TABLE `detail_pemeriksaan` (
  `id` int(5) NOT NULL,
  `id_hasil_pemeriksaan` int(5) NOT NULL,
  `id_jenis_pemeriksaan` int(5) NOT NULL,
  `id_user_penginput` int(5) NOT NULL,
  `hasil` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pemeriksaan`
--

INSERT INTO `detail_pemeriksaan` (`id`, `id_hasil_pemeriksaan`, `id_jenis_pemeriksaan`, `id_user_penginput`, `hasil`) VALUES
(1, 19, 1, 1, 'hfisdfihsifisd'),
(2, 9, 1, 5, 'mata sehat'),
(3, 3, 1, 5, 'ef fw i '),
(4, 3, 1, 5, 'ththththth'),
(5, 3, 1, 5, 'fgffg'),
(6, 3, 1, 5, 'fgffg'),
(7, 3, 1, 5, 'fgffg'),
(8, 3, 1, 5, 'dfffd'),
(9, 3, 1, 5, 'fgfgfgf'),
(10, 3, 1, 5, 'ds'),
(11, 3, 1, 5, 'ds'),
(12, 3, 1, 5, 'ds'),
(13, 3, 1, 5, 'd'),
(14, 3, 1, 5, 'dfdfdf'),
(15, 3, 1, 5, 'ggfg'),
(16, 3, 1, 5, 'ggfg'),
(17, 3, 1, 5, 'fghfgh'),
(18, 3, 1, 5, 'ghfghfghf'),
(19, 3, 1, 5, 'dd'),
(20, 1, 1, 5, 'jhgjfytfuf'),
(21, 1, 1, 5, 'dffdf'),
(22, 1, 1, 5, 'dffdf'),
(23, 1, 1, 5, 'dffdf'),
(24, 1, 1, 5, 'dsdsdsd'),
(25, 1, 1, 5, 'dsdsdsd'),
(26, 1, 1, 5, 'dsdsdsd'),
(27, 1, 1, 5, 'ffdfdf'),
(28, 1, 1, 5, 'ffdfdf'),
(29, 15, 1, 5, 'sddfsdfs'),
(30, 1, 1, 5, 'fgdfgf'),
(31, 1, 1, 5, 'tgrt'),
(32, 1, 1, 5, 'dfdfgd'),
(33, 1, 1, 5, 'dfdfdf');

-- --------------------------------------------------------

--
-- Table structure for table `harga_pemeriksaan`
--

CREATE TABLE `harga_pemeriksaan` (
  `id` int(11) NOT NULL,
  `nama_pemeriksaan` varchar(30) DEFAULT NULL,
  `kategori` varchar(50) DEFAULT NULL,
  `harga_pemeriksaan` int(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hasil_pemeriksaan`
--

CREATE TABLE `hasil_pemeriksaan` (
  `id` int(5) NOT NULL,
  `id_pasien` int(5) NOT NULL,
  `total_bayar` int(10) NOT NULL,
  `status_bayar` int(11) NOT NULL,
  `tanggal_periksa` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_pemeriksaan`
--

INSERT INTO `hasil_pemeriksaan` (`id`, `id_pasien`, `total_bayar`, `status_bayar`, `tanggal_periksa`) VALUES
(1, 2, 130000, 0, '0000-00-00 00:00:00'),
(2, 2, 0, 0, '0000-00-00 00:00:00'),
(3, 1, 10000, 0, '0000-00-00 00:00:00'),
(4, 3, 0, 0, '0000-00-00 00:00:00'),
(9, 1, 0, 0, '0000-00-00 00:00:00'),
(15, 1, 3, 0, '0000-00-00 00:00:00'),
(16, 1, 0, 0, '2016-11-08 00:00:00'),
(17, 1, 0, 0, '2016-11-08 06:42:06'),
(18, 1, 0, 0, '2016-11-09 07:57:02'),
(19, 1, 0, 0, '2016-11-09 07:57:36'),
(20, 1, 0, 0, '2016-11-10 03:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pemeriksaan`
--

CREATE TABLE `jenis_pemeriksaan` (
  `id` int(5) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `harga` int(15) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pemeriksaan`
--

INSERT INTO `jenis_pemeriksaan` (`id`, `nama`, `harga`, `deskripsi`) VALUES
(1, 'periksa mata', 10000, 'periksa mata sehat atau tidak');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `authKey` varchar(50) DEFAULT NULL,
  `accessToken` varchar(50) DEFAULT NULL,
  `role` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `authKey`, `accessToken`, `role`) VALUES
(1, 'mursit', 'bismillah', 'mursit-12345', 'mumu2937412912zzzz', 'Admin'),
(2, 'admin', 'admin', 'admin-12345', 'admi2937412912zzzz', 'Admin'),
(4, 'adit', 'bismillah', 'mahasiswa-1234', 'mahasiswa647236283234bfbjed23', 'Mahasiswa'),
(5, 'stafflab', 'stafflab', '12345', '54321', '1'),
(6, 'dokter', 'dokter', '123453', '534554535', '2');

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `nim` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `pekerjaan` varchar(30) NOT NULL,
  `keluhan` varchar(50) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`nim`, `nama`, `pekerjaan`, `keluhan`, `alamat`) VALUES
('1', 'sada', 'supir', 'kejawan', 'tmb 100'),
('2', 'nini', 'guru', 'bali', 'keputih');

-- --------------------------------------------------------

--
-- Table structure for table `status_bayar`
--

CREATE TABLE `status_bayar` (
  `id` int(5) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pemeriksaan`
--
ALTER TABLE `detail_pemeriksaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harga_pemeriksaan`
--
ALTER TABLE `harga_pemeriksaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hasil_pemeriksaan`
--
ALTER TABLE `hasil_pemeriksaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pemeriksaan`
--
ALTER TABLE `jenis_pemeriksaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `status_bayar`
--
ALTER TABLE `status_bayar`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pemeriksaan`
--
ALTER TABLE `detail_pemeriksaan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `harga_pemeriksaan`
--
ALTER TABLE `harga_pemeriksaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hasil_pemeriksaan`
--
ALTER TABLE `hasil_pemeriksaan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `jenis_pemeriksaan`
--
ALTER TABLE `jenis_pemeriksaan`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `status_bayar`
--
ALTER TABLE `status_bayar`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
