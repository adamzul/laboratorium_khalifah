<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Sistem Informasi Laboratorium';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Selamat Datang</h1>

        <p class="lead">Sistem Informasi Laboratorium</p>
        <?php if(Yii::$app->user->isGuest) {?>
        <p><?= Html::a('login first',['login'], ['class'=>'btn btn-success']) ?></p>
        <?php } ?>
   </div>

    <!-- <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Daftar Pasien</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><?= Html::a('pasien',['pasien/index'],['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Daftar Tes Laborat</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><?= Html::a('daftar tes',['jenis-pemeriksaan/index'],['class'=>'btn btn-default']) ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Cek Hasil</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><?= Html::a('cek hasil',['hasil-pemeriksaan/index'],['class'=>'btn btn-default']) ?> </p>
            </div>
            <div class="col-lg-4">
                <h2>Cek Detail Hasil</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><?= Html::a('cek datail hasil',['detail-pemeriksaan/index'],['class'=>'btn btn-default']) ?> </p>
            </div>
        </div>

    </div> -->
</div>
