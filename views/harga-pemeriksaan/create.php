<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HargaPemeriksaan */

$this->title = 'Create Harga Pemeriksaan';
$this->params['breadcrumbs'][] = ['label' => 'Harga Pemeriksaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="harga-pemeriksaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
