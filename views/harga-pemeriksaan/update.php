<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HargaPemeriksaan */

$this->title = 'Update Harga Pemeriksaan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Harga Pemeriksaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="harga-pemeriksaan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
