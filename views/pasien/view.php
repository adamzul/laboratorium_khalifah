<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\grid\GridView;



/* @var $this yii\web\View */
/* @var $model app\models\Pasien */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Pasiens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pasien-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->nim], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->nim], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('lihat hasil pemeriksaan', ['hasil-pemeriksaan/indexpemeriksaan','param'=>$model->nim], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model2, 'id_pasien')->hiddenInput(['value' => $model->nim])->label(false) ?>

            <?= $form->field($model2, 'total_bayar')->hiddenInput(['value' => 0])->label(false) ?>


            <?= $form->field($model2, 'tanggal_periksa')->hiddenInput(['value' => date("Y-m-d h:i:sa")])->label(false) ?>

    

    <div class="form-group">
        <?= Html::submitButton($model2->isNewRecord ? 'buat pemeriksaan baru' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nim',
            'nama',
            'pekerjaan',
            'keluhan',
            'alamat:ntext',
        ],
    ]) ?>

    <?php
    $buttonNew=1;
    $gridColumns = [
                'class'=>'yii\grid\ActionColumn',
                'headerOptions'=>['class'=>'kartik-sheet-style'],
                'template' => '{my_button}', 
                'buttons' => [
                    'my_button' => function ($url, $dataProvider) {
                    return Html::a('tambah ', ['detail-pemeriksaan/create','param'=>$dataProvider->id],['class' => 'btn btn-success']);
                },
                ],
            ];
            ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_pasien',
            'total_bayar',
            'status_bayar',
            'tanggal_periksa',
            $gridColumns,

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
