<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HasilPemeriksaan */

$this->title = 'Create Hasil Pemeriksaan';
$this->params['breadcrumbs'][] = ['label' => 'Hasil Pemeriksaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hasil-pemeriksaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
