<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Pasien;
use yii\db\Query;
/* @var $this yii\web\View */
/* @var $searchModel app\models\HasilPemeriksaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Hasil Pemeriksaans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hasil-pemeriksaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php echo var_dump($dataProvider); ?>
    <!-- <p>
        <?= Html::a('Create Hasil Pemeriksaan', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
    <?php 
    $gridColumns= [
                'class'=>'yii\grid\ActionColumn',
                'headerOptions'=>['class'=>'kartik-sheet-style'],
                'template' => '{my_button}', 
                'buttons' => [
                    'my_button' => function ($url, $dataProvider) {
                    return Html::a('tambah ', ['detail-pemeriksaan/create','param'=>$dataProvider->id],['class' => 'btn btn-success']);
                },
                ],
            ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'id_pasien',
            'pasien.nama',

            // [
            //     'attribute' => 'some_title',
            //     'format' => 'raw',
            //     'value' => function ($dataProvider) {   
            //             return  1;
            //     },
            // ],
            'total_bayar',
            'status_bayar',
            'tanggal_periksa',
            $gridColumns,
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
