<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Pasien;

/* @var $this yii\web\View */
/* @var $model app\models\HasilPemeriksaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hasil-pemeriksaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_pasien')->dropDownList(Pasien::find()->select('nim','nama')->indexBy('nim')->column()) ?>

    <?= $form->field($model, 'total_bayar')->textInput() ?>

    <?= $form->field($model, 'tanggal_periksa')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
