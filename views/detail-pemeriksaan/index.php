<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DetailPemeriksaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Detail Pemeriksaans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-pemeriksaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Detail Pemeriksaan', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_hasil_pemeriksaan',
            'id_jenis_pemeriksaan',
            'id_user_penginput:ntext',
            'hasil',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
