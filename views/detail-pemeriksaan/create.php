<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DetailPemeriksaan */

$this->title = 'Create Detail Pemeriksaan';
$this->params['breadcrumbs'][] = ['label' => 'Detail Pemeriksaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detail-pemeriksaan-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'hasilPemeriksaan' => $hasilPemeriksaan,
        'param' => $param,
                'arrayOfId' => $arrayOfId,
        
    ]) ?>

</div>
