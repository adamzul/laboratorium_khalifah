<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\HasilPemeriksaan;
use app\models\JenisPemeriksaan;

/* @var $this yii\web\View */
/* @var $model app\models\DetailPemeriksaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detail-pemeriksaan-form">
	<?php echo var_dump($arrayOfId); ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_hasil_pemeriksaan')->hiddenInput(['value' => $param])->label(false) ?>

    <?= $form->field($model, 'id_jenis_pemeriksaan')->dropDownList(JenisPemeriksaan::find()->select('nama','nama')->indexBy('id')->column()) ?>

    <?= $form->field($model, 'id_user_penginput')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>

    <?= $form->field($model, 'hasil')->textarea(['rows' => 6]) ?>

    
  



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
   <!--  -->

</div>
