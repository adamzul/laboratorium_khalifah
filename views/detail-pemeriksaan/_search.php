<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DetailPemeriksaanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detail-pemeriksaan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_hasil_pemeriksaan') ?>

    <?= $form->field($model, 'id_jenis_pemeriksaan') ?>

    <?= $form->field($model, 'id_user_penginput') ?>

    <?= $form->field($model, 'hasil') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
