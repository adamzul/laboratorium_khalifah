<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JenisPemeriksaan */

$this->title = 'Create Jenis Pemeriksaan';
$this->params['breadcrumbs'][] = ['label' => 'Jenis Pemeriksaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-pemeriksaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
