<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "harga_pemeriksaan".
 *
 * @property integer $id
 * @property string $nama_pemeriksaan
 * @property string $kategori
 * @property integer $harga_pemeriksaan
 * @property string $keterangan
 */
class HargaPemeriksaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'harga_pemeriksaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['harga_pemeriksaan'], 'integer'],
            [['nama_pemeriksaan'], 'string', 'max' => 30],
            [['kategori', 'keterangan'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_pemeriksaan' => 'Nama Pemeriksaan',
            'kategori' => 'Kategori',
            'harga_pemeriksaan' => 'Harga Pemeriksaan',
            'keterangan' => 'Keterangan',
        ];
    }
}
