<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HasilPemeriksaan;
use yii\db\Query;

/**
 * HasilPemeriksaanSearch represents the model behind the search form about `app\models\HasilPemeriksaan`.
 */
class HasilPemeriksaanSearch extends HasilPemeriksaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pasien' ,'total_bayar', 'status_bayar'], 'integer'],
            [['total_bayar', 'status_bayar','tanggal_periksa'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id=null)
    {
        // $tes=new Query();
        // $query= $tes->createCommand(select * from hasil_pemeriksaan);
        // $query = HasilPemeriksaan::find()->orderBy('tanggal_periksa');
        $query2= new Pasien;
        $query = HasilPemeriksaan::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if(!$id == null)
        $query->andFilterWhere([
            'hasil_pemeriksaan.id' => $this->id,
            'id_pasien' => $id,
            'total_bayar' => $this->total_bayar,
            'status_bayar' => $this->status_bayar,
            'tanggal_periksa' => $this->tanggal_periksa,
        ]);
        else
            $query->andFilterWhere([
            'id' => $this->id,
            'id_pasien' => $this->id_pasien,
            'total_bayar' => $this->total_bayar,
            'status_bayar' => $this->status_bayar,
            'tanggal_periksa' => $this->tanggal_periksa,
        ]);        
        return $dataProvider;
    }

     public function search2($params)
    {
        $query = HasilPemeriksaan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pasien' => $params,
        ]);

        return $dataProvider;
    }
}
