<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasien".
 *
 * @property string $nim
 * @property string $nama
 * @property string $pekerjaan
 * @property string $keluhan
 * @property string $alamat
 */
class Pasien extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pasien';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nim', 'nama', 'pekerjaan', 'keluhan', 'alamat'], 'required'],
            [['alamat'], 'string'],
            [['nim'], 'string', 'max' => 10],
            [['nama', 'keluhan'], 'string', 'max' => 50],
            [['pekerjaan'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nim' => 'Nim',
            'nama' => 'Nama',
            'pekerjaan' => 'Pekerjaan',
            'keluhan' => 'Keluhan',
            'alamat' => 'Alamat',
        ];
    }

    public function getHasilPemeriksaan()
    {
        return $this->hasMany(HasilPemeriksaan::className(), ['id_pasien' => 'nim']);
    }
}
