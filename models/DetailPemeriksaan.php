<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "detail_pemeriksaan".
 *
 * @property integer $id
 * @property integer $id_hasil_pemeriksaan
 * @property integer $id_jenis_pemeriksaan
 * @property integer $id_user_penginput
 * @property string $hasil
 */
class DetailPemeriksaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail_pemeriksaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_hasil_pemeriksaan', 'id_jenis_pemeriksaan', 'id_user_penginput', 'hasil'], 'required'],
            [['id_hasil_pemeriksaan', 'id_jenis_pemeriksaan', 'id_user_penginput'], 'integer'],
            [['hasil'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_hasil_pemeriksaan' => 'Id Hasil Pemeriksaan',
            'id_jenis_pemeriksaan' => 'Id Jenis Pemeriksaan',
            'id_user_penginput' => 'Id User Penginput',
            'hasil' => 'Hasil',
        ];
    }
}
