<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "view_hasil_pemeriksaan".
 *
 * @property integer $id
 * @property integer $id_pasien
 * @property string $nama
 * @property integer $total_bayar
 * @property integer $status_bayar
 * @property string $tanggal_periksa
 */
class ViewHasilPemeriksaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view_hasil_pemeriksaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_pasien', 'total_bayar', 'status_bayar'], 'integer'],
            [['id_pasien', 'nama', 'total_bayar', 'status_bayar', 'tanggal_periksa'], 'required'],
            [['tanggal_periksa'], 'safe'],
            [['nama'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pasien' => 'Id Pasien',
            'nama' => 'Nama',
            'total_bayar' => 'Total Bayar',
            'status_bayar' => 'Status Bayar',
            'tanggal_periksa' => 'Tanggal Periksa',
        ];
    }

    public function primaryKey()
    {
            return 'id';
    }

}
