<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HargaPemeriksaan;

/**
 * HargaPemeriksaanSearch represents the model behind the search form about `app\models\HargaPemeriksaan`.
 */
class HargaPemeriksaanSearch extends HargaPemeriksaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'harga_pemeriksaan'], 'integer'],
            [['nama_pemeriksaan', 'kategori', 'keterangan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HargaPemeriksaan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'harga_pemeriksaan' => $this->harga_pemeriksaan,
        ]);

        $query->andFilterWhere(['like', 'nama_pemeriksaan', $this->nama_pemeriksaan])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
