<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_pemeriksaan".
 *
 * @property integer $id
 * @property string $nama
 * @property integer $harga
 * @property string $deskripsi
 */
class JenisPemeriksaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_pemeriksaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'harga', 'deskripsi'], 'required'],
            [['harga'], 'integer'],
            [['deskripsi'], 'string'],
            [['nama'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'harga' => 'Harga',
            'deskripsi' => 'Deskripsi',
        ];
    }
}
