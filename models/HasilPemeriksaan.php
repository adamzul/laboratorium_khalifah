<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hasil_pemeriksaan".
 *
 * @property integer $id
 * @property integer $id_pasien
 * @property integer $total_bayar
 * @property string $tanggal_periksa
 */
class HasilPemeriksaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hasil_pemeriksaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pasien',  'total_bayar'], 'required'],
            [['id_pasien', 'total_bayar'], 'integer'],
            [['total_bayar', 'status_bayar','tanggal_periksa'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pasien' => 'Id Pasien',
            'total_bayar' => 'Total Bayar',
            'status_bayar' => 'Status Bayar',
            'tanggal_periksa' => 'Tanggal Periksa',
        ];
    }

    public function getPasien()
    {
        return $this->hasOne(Pasien::className(), ['nim' => 'id_pasien']);
    }
}
